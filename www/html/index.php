<?php
include ('b4.php');
?>

<?php

//se inicializa una sesión
session_start();
//se manda llamar el archivo de conexión
require_once 'cnx.php';
//se verifica si se presiona el botón llamado validar
if (isset($_POST['inicio']))
{
  //la función ayuda a evitar inyección de sql
  //se guarda en las variables$us y $ps
  $us=$_POST['usuario'];
  $ps=$_POST['contrasena'];
  
  //Query de consulta
  $query = $cnn->prepare('SELECT * from usuarios WHERE usuario =:usuario AND contrasena=:contrasena');

  //Manejo de parámetros
  $query->bindParam(':usuario', $us);
  $query->bindParam(':contrasena', $ps);
  
  //Execución del query
  $query->execute(); 
  $count=$query->rowCount();
  $campo= $query->fetch();
  
  if($count)  
  {
    $_SESSION['nombre'] = $campo['nombre'];
    $_SESSION['usuario'] = $campo['usuario'];
    $_SESSION['email'] = $campo['email'];
    $_SESSION['ciudad'] = $campo['ciudad'];
    $_SESSION['peso'] = $campo['peso'];
    $_SESSION['altura'] = $campo['altura'];
    $_SESSION['edad'] = $campo['edad'];
    $_SESSION['genero'] = $campo['genero'];
    $_SESSION['telefono'] = $campo['telefono'];
    $_SESSION['rol'] = $campo['rol'];
    if (strcmp($campo['rol'], "administrador") == 0){
      header("location:vistaadmin.php");
    }
    else if (strcmp($campo['rol'], "usuario") == 0){
      header("location:index.php");
    }
    else if (strcmp($campo['rol'], "Entrenador") == 0){
      header("location:vistaentrenador.php");
    }
    else if (strcmp($campo['rol'], "Nutriologo") == 0){
      header("location:vistanutriologo.php");
    }
  } 
  else 
  {
    header("location:inicio.php"); 
    //echo "Verifica las credenciales de acceso";
    //echo "<br><a href='login.php'>Regresar</a>";
  }
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Elite Fitness</title>
    <link rel="icon" href="images/Icon.png">
    <script src="https://kit.fontawesome.com/812d3c2faa.js" crossorigin="anonymous"></script>
</head>

<style>
  .navbar-brand
{
    width:100%;
    left:0;
    text-align:left;
    margin:auto;
}
</style>

<!-- Barra de navegación -->
  <ul style="list-style-type: none; margin: 0; padding: 0; overflow: hidden; background-color: #000;">
      <li style="float: left;"><a class="active" href="/EliteFitness/home.php" style="display: block; color: white; text-align: center; padding: 19px; text-decoration: none;"><img src="images/Logo3.png"  width="220" height="50"></a></li>
  </ul>

<body style="background-image: url(images/fitness0.jpeg); background-attachment: fixed; background-size: 100%">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-2"></div>
      <div class="col-sm-8">
        <div class="container-fluid" style="margin-top: 50px; background-color: purple; padding: 25px; border-color: white; border-top-right-radius: 30px;">
          <h3 align="center" style="color: white"><strong>Inicia tu sesión aquí para entrar a esta Elite</strong></h3>
        </div>
        <div class="container-fluid" style=" margin-bottom: 50px; background-color: #f1f2f6; padding: 25px; border-color: white;  border-bottom-left-radius: 30px;">
          <form id="inicioSesion" method="post">
            <div class="row" style="margin-bottom: 0px">
              <div class="col-sm-6">
                <img src="images/fitness2.jpeg" style="width: 100%; height: auto; border-radius: 10px">
              </div>
              <div class="col-sm-6"><br>
                <label><i class="fas fa-user-circle"></i> <strong>Usuario:</strong></label>
                <input type="text" name="usuario"  class="form-control" autofocus><br>
                <label><i class="fas fa-key"></i> <strong>Contraseña:</strong></label>
                <input type="password" name="contrasena"  class="form-control">
                <div style="margin-top: 10px; margin-bottom: 10px;" align="right">
                  <a href="/EliteFitness/registro.php" style="text-decoration: none; color: purple; text-align: right;"><strong>¿No tienes cuenta aún?</strong></a><br>
                    <button type="submit" class="btn" name="inicio" style="background-color: purple; color: white; margin-top: 10px">
                      <i class="far fa-arrow-alt-circle-right"></i> Iniciar
                    </button>
                </div>
              </div>
            </div>
          </form>
        </div>      
      </div>
      <div class="col-sm-2"></div>
    </div>    
  </div>

</body>

</html>

<!-- Scripts del Tooltip -->
<script>
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();   
});
</script>