<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<?php
/* Conectar a una base de datos de MySQL invocando al controlador */
$db_host = 'mysql:dbname=elitefitness;host=localhost';
$usuario = 'root';
$contrasena = '';

try {
    $cnn = new PDO($db_host, $usuario, $contrasena);
} catch (PDOException $e) {
   echo "<div class='alert alert-danger alert-dismissible'>
    		<button type='button' class='close' data-dismiss='alert'>&times;</button>
    			<strong>Peligro!</strong> <strong><h4>Ha surgido un error y no se puede conectar a la base de datos!</strong><br><br> Detalle:<br>." . $e->getMessage()."
         </div>";
}
?>